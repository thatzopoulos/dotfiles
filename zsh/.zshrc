# _______| |__  _ __ ___ 
#|_  / __| '_ \| '__/ __|
# / /\__ \ | | | | | (__ 
#/___|___/_| |_|_|  \___|
# Lines configured by zsh-newuser-installnes configured by zsh-newuser-install

zmodload zsh/zprof

OS="$(uname -s)"

# set autoload path
fpath=(~/.zsh-functions $fpath)

# trying to load custom functions
autoload -Uz bcp  bip  bup  fp  kp  ks  ll  lps  lx  tmuxify  utils  vmc  vmi 

# ditch duplicate path entries on loading of .zshrc
typeset -U PATH fpath

#export CLICOLOR=1

HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
setopt hist_verify
setopt inc_append_history
setopt share_history
setopt hist_ignore_dups
setopt hist_ignore_all_dups
setopt hist_expire_dups_first
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/thatzopoulos/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

export EDITOR='vim'
source ~/.zsh_aliases
source ~/.zsh_secrets

#FZF STUFF
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

#pythonrc source file PYTHONPATH
export PYTHONSTARTUP=~/.pythonrc

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=5

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"


# NVM takes forever to load so we use zsh-nvm to lazily load it
# Must export these envvars before installing the plugin
if test "$OS" = "Darwin"; then
    export NVM_DIR="$HOME/.nvm"
    export NVM_LAZY_LOAD=true
    export NVM_COMPLETION=true
fi

### Antidote ###
# clone antidote if necessary
[[ -e ~/.antidote ]] || git clone https://github.com/mattmc3/antidote.git ~/.antidote

# source antidote
. ~/.antidote/antidote.zsh

# generate and source plugins from ~/.zsh_plugins.txt
antidote load

### Pip Stuff ###
export PIP_REQUIRE_VIRTUALENV=false
gpip3(){
   PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
}

gpip2(){
   PIP_REQUIRE_VIRTUALENV="" pip2 "$@"
}


# Direnv hook
if command -v direnv 1>/dev/null 2>&1; then eval "$(direnv hook zsh)"; fi

# Pyenv hook
# Load pyenv into the shell by adding
# the following to ~/.zshrc:
eval "$(pyenv init -)"

# Pyenv virtualenv hook
if command -v pyenv-virtualenv-init 1>/dev/null 2>&1; then eval "$(pyenv virtualenv-init -)"; fi

#Global Variables
export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'


# GO STUFF
export GOPATH=$HOME/go # don't forget to change your path correctly!
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:$GOROOT/bin

# Work Stuff

if test "$OS" = "Darwin"; then

    export PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"
    export PATH="/opt/homebrew/opt/grep/libexec/gnubin:$PATH"
    eval "$(/opt/homebrew/bin/brew shellenv)"
    export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"
    export PATH="/usr/local/opt/ruby/bin:$PATH"
    export PATH="/usr/local/opt/node@8/bin:$PATH:/usr/local/bin"
    # Homebrew stuff so that i can have gcc in my path before apples fake gcc
    export PATH=/usr/local/bin:$PATH

    # Add Visual Studio Code (code)
    export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"

    # Get git autocomplete for mac
    if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

    autoload -Uz compinit
    compinit
    fi

    # postgresql 13 stuff
    export PATH="/opt/homebrew/opt/postgresql@13/bin:$PATH"
fi



# plan 9 port stuff
export PLAN9=/usr/local/plan9
export PATH=$PATH:$PLAN9/bin
export BROWSER="chromium"


# Other
# echo $(fortune -e) \\n $(business-time) | cowsay -f dragon | lolcat
fortune -e | cowsay -f dragon | lolcat
# Functions
# testing speed of zsh startup
    timezsh() {
    shell=${1-$SHELL}
    for i in $(seq 1 10); do time $shell -i -c exit; done
    }

# Some of the most useful features in emacs-libvterm require shell-side
# configurations. The main goal of these additional functions is to enable the
# shell to send information to `vterm` via properly escaped sequences. A
# function that helps in this task, `vterm_printf`, is defined below.

function vterm_printf(){
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

# Completely clear the buffer. With this, everything that is not on screen
# is erased.s
if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput sclear'
fi

# With vterm_cmd you can execute Emacs commands directly from the shell.
# For example, vterm_cmd message "HI" will print "HI".
# To enable new commands, you have to customize Emacs's variable
# vterm-eval-cmds.
vterm_cmd() {
    local vterm_elisp
    vterm_elisp=""
    while [ $# -gt 0 ]; do
        vterm_elisp="$vterm_elisp""$(printf '"%s" ' "$(printf "%s" "$1" | sed -e 's|\\|\\\\|g' -e 's|"|\\"|g')")"
        shift
    done
    vterm_printf "51;E$vterm_elisp"
}

# FZF Functions
# Use ripgrepall interactively via fzf
rga-fzf() {
	RG_PREFIX="rga --files-with-matches"
	local file
	file="$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
			fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
				--phony -q "$1" \
				--bind "change:reload:$RG_PREFIX {q}" \
				--preview-window="70%:wrap"
	)" &&
	echo "opening $file" &&
	xdg-open "$file"
}
# fbr - checkout git branch (including remote branches), sorted by most recent commit, limit 30 last branches
fbr() {
  local branches branch
  branches=$(git for-each-ref --count=30 --sort=-committerdate refs/heads/ --format="%(refname:short)") &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}
# fco_preview - checkout git branch/tag, with a preview showing the commits between the tag/branch and HEAD
fco_preview() {
  local tags branches target
  branches=$(
    git --no-pager branch --all \
      --format="%(if)%(HEAD)%(then)%(else)%(if:equals=HEAD)%(refname:strip=3)%(then)%(else)%1B[0;34;1mbranch%09%1B[m%(refname:short)%(end)%(end)" \
    | sed '/^$/d') || return
  tags=$(
    git --no-pager tag | awk '{print "\x1b[35;1mtag\x1b[m\t" $1}') || return
  target=$(
    (echo "$branches"; echo "$tags") |
    fzf --no-hscroll --no-multi -n 2 \
        --ansi --preview="git --no-pager log -150 --pretty=format:%s '..{2}'") || return
  git checkout $(awk '{print $2}' <<<"$target" )
}
# fcoc - checkout git commit
fcoc() {
  local commits commit
  commits=$(git log --pretty=oneline --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e) &&
  git checkout $(echo "$commit" | sed "s/ .*//")
}
alias glNoGraph='git log --color=always --format="%C(auto)%h%d %s %C(black)%C(bold)%cr% C(auto)%an" "$@"'
_gitLogLineToHash="echo {} | grep -o '[a-f0-9]\{7\}' | head -1"
_viewGitLogLine="$_gitLogLineToHash | xargs -I % sh -c 'git show --color=always %'"

# fcoc_preview - checkout git commit with previews
fcoc_preview() {
  local commit
  commit=$( glNoGraph |
    fzf --no-sort --reverse --tiebreak=index --no-multi \
        --ansi --preview="$_viewGitLogLine" ) &&
  git checkout $(echo "$commit" | sed "s/ .*//")
}

# fshow_preview - git commit browser with previews
fshow_preview() {
    glNoGraph |
        fzf --no-sort --reverse --tiebreak=index --no-multi \
            --ansi --preview="$_viewGitLogLine" \
                --header "enter to view, alt-y to copy hash" \
                --bind "enter:execute:$_viewGitLogLine   | less -R" \
                --bind "alt-y:execute:$_gitLogLineToHash | xclip"
}
# gh-watch -- watch the current actions
gh-watch() {
 gh run list --branch $(git rev-parse --abbrev-ref HEAD) --json status,name,databaseId |
 jq -r '.[] | select(.status != "completed") | (.databaseId | tostring) + "\t" + (.name)' |
 fzf -1 -0 | awk '{print $1}' | xargs gh run watch
}
# Preview and checkout prs that ive been assinged to review
pr () {
    local gh_user=${1:-}

    if [ -z "$gh_user" ]; then
    gh_user="@me"
    fi

    tmpFile=$(mktemp /tmp/prs-XXXXXX.json)

    cleanup () {
    rm "$tmpFile"
    }

    trap cleanup EXIT
    gh pr list -S "review-requested:${gh_user}" \
    --json number,title,headRefName,body,author,isDraft \
    | jq -r "$jq_query" > "$tmpFile"

    preview_command="\
    jq -r '.[\"{1}\"].body' $tmpFile \
    | pandoc -f gfm -t markdown      \
    | glow -s light -
    "
    pr_number=$(
    jq -r \
        'to_entries | map(.value.line) | join("
")' \
        "$tmpFile" \
    | column -t -s: \
    | fzf                            \
        --ansi                       \
        --delimiter=' '              \
        --preview="$preview_command" \
        --preview-window=up:80%      \
    | grep -o '^[0-9]\+'
    )

    if [ -n "$pr_number" ]; then
    errcho "checking out #$pr_number"
    gh pr checkout "$pr_number" && gh pr view --web "$pr_number" && git pull || echo -e "gh pr checkout $pr_number && gh pr view --web $pr_number && git pull"
    else
    errcho "canceled"
    fi
}

# This is to change the title of the buffer based on information provided by the
# shell. See, http://tldp.org/HOWTO/Xterm-Title-4.html, for the meaning of the
# various symbols.
autoload -U add-zsh-hook
add-zsh-hook -Uz chpwd (){ print -Pn "\e]2;%m:%2~\a" }

# Sync directory and host in the shell with Emacs's current directory.
# You may need to manually specify the hostname instead of $(hostname) in case
# $(hostname) does not return the correct string to connect to the server.
#
# The escape sequence "51;A" has also the role of identifying the end of the
# prompt
# vterm_prompt_end() {
#     vterm_printf "51;A$(whoami)@$(hostname):$(pwd)";
# }
# setopt PROMPT_SUBST
# PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'

[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
