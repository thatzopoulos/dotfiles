#!/bin/sh

stow git-config-personal
stow zsh
stow kitty
stow tmux
stow alacritty-mac
stow vim
stow ranger
stow karabiner
