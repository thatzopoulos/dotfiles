#!/bin/sh

# install homebrew
# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# install rust
# curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

brew install kitty tmux alacritty ranger fzf cowsay fortune zsh alacritty stow htop gotop bat pyenv lolcat just cmake libtool direnv
brew tap homebrew/cask-fonts
brew install --cask font-iosevka
brew install hammerspoon --cask
brew tap jimeh/emacs-builds
brew install --cask emacs-app-nightly

# tmux plugins cloning
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

# setup rls for rust after rust is installed
git clone https://github.com/rust-analyzer/rust-analyzer.git && cd rust-analyzer
cargo xtask install --server

# setup fzf auto completions
/opt/homebrew/opt/fzf/install

#Setup ssh before this step
git clone git@gitlab.com:thatzopoulos/dotfiles.git
git clone git@gitlab.com:thatzopoulos/emacs.d ~/.emacs.d
git clone git@gitlab.com:thatzopoulos/scripts ~/scripts

stow zsh
stow kitty
stow tmux
stow alacritty-mac
stow vim
stow ranger
stow karabiner
stow gitconfig-mac
stow hammerspoon
