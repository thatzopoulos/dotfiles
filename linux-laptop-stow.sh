#!/bin/sh

declare -a arr=("beets" "i3" "ranger" "tmux" "vim" "zsh" "kitty" "i3" "alacritty-linux" "polybar")

for i in "${arr[@]}"
do
   echo "$i"
   stow "$i"
done

# Can also access them using echo "${arr[0]}", "${arr[1]}"
