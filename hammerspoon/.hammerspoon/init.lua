
-- preload stuff
require "preload"
-----------------------------------------------
-- Set up
-----------------------------------------------

-- local hyper = {"shift", "cmd", "alt", "ctrl"}
local hyper = {"shift", "cmd"}

hs.window.animationDuration = 0 -- disable animations

require("hs.application")
require("hs.window")


local fnutils = require "hs.fnutils"
local partial = fnutils.partial
local indexOf = fnutils.indexOf
local filter = fnutils.filter

-- require "fntools"
-- require "extensions"
require "app_cycle"
require "mouse"
require "keyboard"
require "switchWindows"
-- require "switchEditor"

hs.crash.crashLogToNSLog = true


-----------------------------------------------
-- Functions
-----------------------------------------------

function alert(msg)
    hs.alert.show(msg)
end

--
--  Directions
--

function _win_full(max)
    return {x = max.x, y = max.y, w = max.w, h = max.h}
end


function _win_left(max)
    return {x = max.x, y = max.y, w = max.w / 2, h = max.h}
end

function _win_right(max)
    return {
        x = max.x + (max.w / 2), y = max.y,
        w = max.w / 2, h = max.h
    }
end

function _win_top(max)
    return {x = max.x, y = max.y, w = max.w, h = max.h / 2}
end

function _win_bottom(max)
    return {
        x = max.x, y = max.y + (max.h / 2),
        w = max.w, h = max.h / 2
    }
end

local _resize_direction = {
    full = _win_full,
    left = _win_left,
    right = _win_right,
    top = _win_top,
    bottom = _win_bottom
}


function window_resize(direction)
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        local screen = win:screen()
        local max = screen:frame()

        local frame_rect = _resize_direction[direction](max)
        win:setFrame(frame_rect)
    else
        alert("No focused window.")
    end
end

function window_move_east()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        win:moveOneScreenEast()
        win:setFrame(_win_full(win:screen():frame()))
    else
        alert("No focused window.")
    end
end

function window_move_west()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        win:moveOneScreenWest()
        win:setFrame(_win_full(win:screen():frame()))
    else
        alert("No focused window.")
    end
end

function window_tiling()
    if hs.window.focusedWindow() then
        local win = hs.window.focusedWindow()
        other_win = win:otherWindowsSameScreen()
        -- other_win["0"] = win
        table.insert(other_win, win)
        hs.window.tiling.tileWindows(other_win, _win_full(win:screen():frame()))
    else
        alert("No focused window.")
    end
end

-----------------------------------------------
-- Reload config
-----------------------------------------------

hs.hotkey.bind(hyper, '\\', function()
    hs.reload()
    hs.alert.show("Config loaded.")
    hs.notify.new({
    title = "Hammerspoon", informativeText = "Config reloaded"}):send()
    -- hs.screen:toEast():notify.new({
    --     title="Hammerspoon", informativeText="Config reloaded"}):send()
end)

hs.hotkey.bind(hyper, 'i', win_info())

-----------------------------------------------
--  Keyboard Layout
-----------------------------------------------

hs.hotkey.bind(hyper, "=", showKeyboardLayout)
hs.hotkey.bind(hyper, "0", switchKeyboardLayout)

-----------------------------------------------
--  Window movement
-----------------------------------------------

l = hs.hotkey.modal.new(hyper, 'l') -- layout modal
l:bind({}, 'f', function() window_resize("full") l:exit() end)
hs.hotkey.bind(hyper, "f", function() window_resize("full") end)
hs.hotkey.bind(hyper, "end", function() window_tiling() end)
hs.hotkey.bind(hyper, "Left", function() window_resize("left") end)
hs.hotkey.bind(hyper, "Right", function() window_resize("right") end)
hs.hotkey.bind(hyper, "Up", function() window_resize("top") end)
hs.hotkey.bind(hyper, "Down", function() window_resize("bottom") end)
hs.hotkey.bind(hyper, "1", function() window_move_west() end)
hs.hotkey.bind(hyper, "2", function() window_move_east() end)

-----------------------------------------------
--  App switching
-----------------------------------------------
hs.application.enableSpotlightForNameSearches(true)

-- web editor
hs.hotkey.bind(hyper, "w", launch_or_cycle_focus("Google Chrome"))

-- editor
-- hs.hotkey.bind(hyper, "v", launch_or_cycle_focus("Vs Code"))

-- terminals
-- hs.hotkey.bind(hyper, "t", launch_or_cycle_focus('Alacritty'))

hs.hotkey.bind(hyper, 'j', mouseHighlight)

-----------------------------------------------
--  DEPRECATED APP Switching
-----------------------------------------------
-- single program
-- hs.hotkey.bind(hyper, "w", launch_or_cycle_focus('PyCharm CE'))
-- hs.hotkey.bind(hyper, "w", launch_or_cycle_focus("Sublime Text"))

-- text wrangler...reference...general text editor
-- hs.hotkey.bind(hyper, "r", launch_or_cycle_focus("BBedit"))

-- slack
-- hs.hotkey.bind(hyper, "s", launch_or_cycle_focus("Slack"))

-- help
-- hs.hotkey.bind(hyper, "h", launch_or_cycle_focus("Dash"))

-- api development
-- hs.hotkey.bind(hyper, "a", launch_or_cycle_focus("Postman"))

--hs.hotkey.bind(hyper, "r", launch_or_cycle_focus("RStudio"))

-- hs.hotkey.bind(hyper, "y", launch_or_cycle_focus('Terminal'))

-- math
-- hs.hotkey.bind(hyper, "m", launch_or_cycle_focus('Rstudio'))

-- query, SQL
-- hs.hotkey.bind(hyper, "q", launch_or_cycle_focus('Sequel Pro'))
-- hs.hotkey.bind(hyper, "q", launch_or_cycle_focus('DataGrip'))

-- app groups
-- hs.hotkey.bind(hyper, "b", launch_or_cycle_focus('Google Chrome'))
-- hs.hotkey.bind(hyper, "e", launch_or_cycle_focus('editor'))
-- hs.hotkey.bind(hyper, "t", launch_or_cycle_focus('terminal'))


