
"____    ____  __  .___  ___. .______        ______ 
"\   \  /   / |  | |   \/   | |   _  \      /      |
" \   \/   /  |  | |  \  /  | |  |_)  |    |  ,----'
"  \      /   |  | |  |\/|  | |      /     |  |     
"   \    /    |  | |  |  |  | |  |\  \----.|  `----.
"    \__/     |__| |__|  |__| | _| `._____| \______|

" Thomas Hatzopoulos


"set leader to comma
:let mapleader = ' '
:let maplocalleader = "\\"

"Some Basics
set nocompatible                                " Disable VI compatibility
filetype plugin indent on                       " Automatically detect file types
set showcmd	                  									"Display incomplete commands
syntax on                                     	" Syntax Highlight
set autoread                                    " Re-read file if it was changed on disk
set history=250                                 " More history!

" Set Tab Settings
set shiftwidth=2                                " Use indents of 2 spaces
set expandtab                                   " Tabs are spaces, not tabs
set tabstop=2                                   " An indentation every 2 columns
set softtabstop=2                               " Let backspace delete indent
set autoindent                                  " Indent at the same level of the previous line

set encoding=utf-8
set wildmenu 				                            "Display all matching files when we tab complete
set wildmode=list:longest
set cursorline				                          "Highlight current line
set ttyfast
set ruler					                              " Sets ruler at end of column?
set backspace=indent,eol,start	                " Make backspace work like normal programs
set laststatus=2			                          " 0: Never Display, 1: Only if more than 2 windows, 2: always display statusbar

" Undo File Settings
set undofile
set undodir=~/.vim/undo-dir
set undolevels=250                              " Maximum number of changes that can be undone
set undoreload=500                              " Maximum number lines to save for undo on a buffer reload
set number

"" Searching / Moving
set incsearch	                                  "search as characters are entered
set hlsearch	                                  "highlight matches
set showmatch                                   " Show matching bracket/parenthesis
set ignorecase                                  "searches are case insensitive
set smartcase                                   "unless they contain at least one capital letter
nnoremap <leader><space> :noh<cr>               "clears highlights
nnoremap <tab> %		                            " makes tab key match bracket pairs by pressing tab
vnoremap <tab> %

"" Dealing With Long Lines
set wrap
set textwidth=79
set formatoptions=qrn1
set colorcolumn=80


" Movement
nnoremap j gj	"move vertically by visual line
nnoremap k gk
"Window Pane Movement/ Split Navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"====[ Swap : and ; to make colon commands easier to type ]======
nnoremap  ;  :
" swap esc key with jk when in insert mode
:inoremap jk <esc>

"Search down into subfolders
"Provides tab-completion for all file-related tasks
set path+=**

"enable mouse clicks
set mouse=a

"enable better syntax highlighting
syntax on

"disable autocommenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"" Leader commands
" Strip all trailling whitespace in the current file
nnoremap <leader>W :%s/\s\+$//<cr>:let @/=''<CR>

" reselect text that was just pasted 
nnoremap <leader>v V`]



"Paste selected text
vnoremap <C-v> "+P
"Copy selected text to system clipboard (requires gvim installed):
vnoremap <C-c> "*y :let @+=@*<CR>




" Editing Dotfiles Shortcuts
"Edit Vimrc and Source Vimrc Was :vsplit now :e
:nnoremap <leader>ev :vsplit $MYVIMRC<cr>
:nnoremap <leader>sv :source $MYVIMRC<cr>

"Edit zshrc and Source zshrc
:nnoremap <leader>ez :vsplit ~/.zshrc<cr>
:nnoremap <leader>sz :source ~/.zshrc<cr>

"Edit tmux.conf and Source tmux.conf
:nnoremap <leader>et :vsplit ~/.tmux.conf<cr>
:nnoremap <leader>st :source ~/.tmux.conf<cr>

" Vim Mappings
" Unmap Space from right
nnoremap <Space> <nop>

" ===================== Vim-Plug =======================
" Automatic Install
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" General
Plug 'tpope/vim-commentary'
" Nice comments
Plug 'scrooloose/nerdcommenter'
" File browser
Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'}
# Plug 'christoomey/vim-tmux-navigator'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Easy align stuff
Plug 'junegunn/vim-easy-align'
" Display nice [N/n] when searching
Plug 'google/vim-searchindex'

"" Languages
" Go
Plug 'fatih/vim-go', { 'for': ':go' }

" The Tim Pope Section
" Help with surrounding '"([{ etc
Plug 'tpope/vim-surround'
" change surrounding single quotesto double quotes by using `cs'"`	
Plug 'tpope/vim-surround'			
Plug 'tpope/vim-fugitive'
" Allow repeat of much more
Plug 'tpope/vim-repeat'

" Theming
" Airline statusline
Plug 'vim-airline/vim-airline'
" Some themes for airline and bufferline
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'flazz/vim-colorschemes'

"Git 
Plug 'junegunn/gv.vim'

" Tmux

" Other
call plug#end()

"FZF
" FZF Filename Search Remap to Ctrl+p
nnoremap <C-p> :Files<Cr>
" FZF Within File Search Map to ctrl+f
nnoremap <C-g> :Rg<Cr>
" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

" panes
nnoremap <leader>' :vsp<cr>
set splitright
nnoremap <leader>/ :split<cr>
set splitbelow

" Rust Stuff
let g:rustfmt_autosave = 1

" Vim-Go Settings
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_fmt_command = "goimports"
let g:go_fmt_expiremental = 1
let g:go_term_enabled = 1
" Go Mappings
au FileType go nmap <buffer><silent><localleader>r <Plug>(go-run)
au FileType go nmap <buffer><silent><localleader>i <Plug>(go-info)
au FileType go nmap <buffer><silent><localleader>t <Plug>(go-test-func)
au FileType go nmap <buffer><silent><localleader>d <Plug>(go-doc-vertical)
au FileType go nmap <buffer><silent><localleader>e <Plug>(go-run)
au FileType go nmap <buffer><silent><localleader>f :GoFillStruct<CR>



" Experimental changes
" Code folding options
nmap <leader>f0 :set foldlevel=0<CR>
nmap <leader>f1 :set foldlevel=1<CR>
nmap <leader>f2 :set foldlevel=2<CR>
nmap <leader>f3 :set foldlevel=3<CR>
nmap <leader>f4 :set foldlevel=4<CR>
nmap <leader>f5 :set foldlevel=5<CR>
nmap <leader>f6 :set foldlevel=6<CR>
nmap <leader>f7 :set foldlevel=7<CR>
nmap <leader>f8 :set foldlevel=8<CR>
nmap <leader>f9 :set foldlevel=9<CR>
nmap <leader>f- :set foldmethod=manual<CR>
nmap <leader>f= :set foldmethod=syntax<CR>

" Plugins Config
" Theme 
colorscheme solarized " Nice color scheme
set background=dark    " Set dark for transparent window
set termguicolors      " Set truecolor
" Transaprency {{{
highlight Normal ctermbg=NONE
highlight NonText ctermbg=NONE
" Cursor line coloring
hi CursorLine ctermbg=16 ctermfg=None

" NERDTree 
"set vimtree launch to ctrl + n
nnoremap <C-n> :NERDTreeToggle<CR>
let NERDTreeShowBookmarks=1
let NERDTreeIgnore=['\.py[cd]$', '\~$', '\.swo$', '\.swp$',
	\ '^\.git$', '^\.hg$', '^\.svn$', '\.bzr$']
let NERDTreeChDirMode=0
let NERDTreeQuitOnOpen=1
let NERDTreeMouseMode=2
let NERDTreeShowHidden=1
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_gui_startup=0

" NERDCommenter
" Disable default mappings
let NERDCreateDefaultMappings = 0
" Map toggle comment
map <silent><Leader>c<space> <Plug>NERDCommenterToggle

" Airline {{{
let g:airline_powerline_fonts = 0
"TODO: Which of these?
let g:airline#extensions#bufferline#enabled = 1
let g:airline#extensions#bufferline#enabled = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#fugutive#enabled = 1
let g:airline#extensions#coc#enabled = 1

" Bufferline
let g:bufferline_rotate=1

"CoC
" Extensions could be used for CoC to have more powerful features
" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions << Explanation
" https://www.npmjs.com/search?q=keywords%3Acoc.nvim << Actual list
" use <tab> for trigger completion and navigate to next complete item
function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~ '\s'
endfunction

" Next by Tab
inoremap <silent><expr> <TAB>
		\ pumvisible() ? "\<C-n>" :
		\ <SID>check_back_space() ? "\<TAB>" :
		\ coc#rpc#request('doKeymap', ['snippets-expand', "\<TAB>"])

" Previous by Tab
inoremap <silent><expr><S-Tab>
		\ pumvisible() ? "\<C-p>" :
		\<SID>check_back_space() ? "\<S-Tab>" :
		\ coc#rpc#request('doKeymap', ['snippets-expand', "\<S-Tab>"])

" use <c-space>for trigger completion
imap <expr><c-space> coc#refresh()

" Don't go to new line on enter when completing
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use <C-l> to trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> to select text for visual text of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Show doc
function! s:show_doc() 
	if &filetype == 'vim'
	execute 'h ' . expand('<cword>')
	else
	call CocAction('doHover')
	endif
endfunction
nnoremap <silent> K :call <SID>show_doc()<CR>

" GoTos
nnoremap <silent><leader>gd :call CocAction('jumpDefinition')<CR>
nnoremap <silent><leader>gl :call CocAction('jumpDeclaration')<CR>
nnoremap <silent><leader>gi :call CocAction('jumpImplementation')<CR>
nnoremap <silent><leader>ge :call CocAction('diagnosticList')<CR>

" Other helpful stuff
nnoremap <silent><leader>gc :call CocAction('codeAction')<CR>
nnoremap <silent><leader>gr :call CocAction('rename')<CR>
nnoremap <silent><leader>gq :call CocAction('quickfixes')<CR>
nnoremap <silent><leader>gh :CocList<CR>
" }}}

" Ale
let g:ale_statusline_format = ['E%d', 'W%d', 'K']
let g:ale_lint_delay = 350
let g:ale_sign_column_always = 1
let g:ale_warn_about_trailing_whitespace = 0

" The Tim Pope Corner
" fugitive
nmap <silent><Leader>gw :Gwrite<CR>
" Git status
nmap <silent><Leader>gs :Gstatus<CR>
" Commit staged changes
nmap <silent><Leader>gc :Gcommit<CR>
