
#  | |__   __ _ ___| |__  _ __ ___
#  | '_ \ / _` / __| '_ \| '__/ __|
#  | |_) | (_| \__ \ | | | | | (__
#  |_.__/ \__,_|___/_| |_|_|  \___|


shopt -s autocd #Allows you to cd into directory merely by typing the directory name.
set -o vi
HISTFILESIZE=1000000
HISTSIZE=1000000

#Generic Shortcuts
alias news="newsboat"

#System Maintenance
alias sdn="sudo shutdown now"

#Some Aliases
alias v="vim"
alias sv="sudo vim"
alias r="ranger"
alias sr="sudo ranger"
alias g="git"
alias rf="source ~/.bash_profile"
alias starwars="telnet towel.blinkenlights.nl"
alias ls="ls --color='auto'"
alias p="vim ~/personalWiki.md"
#GNU COREUTILS STUFF
export PATH="$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH"
PATH="/usr/local/opt/coreutils/libexec/gnubin:/thomas.hatzopoulos/local/bin$PATH"
MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"


#ONBOARDING WORK STUFF REQUIRED
#Add to `~/.bashrc` to require pip to have an activated virtual environment
export PIP_REQUIRE_VIRTUALENV=true
gpip3(){
   PIP_REQUIRE_VIRTUALENV="" pip3 "$@"
}

gpip2(){
   PIP_REQUIRE_VIRTUALENV="" pip2 "$@"
}

eval "$(direnv hook bash)"
if command -v pyenv 1>/dev/null 2>&1; then eval "$(pyenv init -)"; fi
eval "$(pyenv virtualenv-init -)"


export PS1="\[\e[35m\][\[\e[m\]\[\e[36m\]\W\[\e[m\]\[\e[35m\]]\[\e[m\] \[\e[32m\]\`parse_git_branch\`\[\e[m\]\n\[\e[36m\]\\$\[\e[m\] "

# get current branch in git repo
function parse_git_branch() {
       BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
       if [ ! "${BRANCH}" == "" ]
       then
               echo "[${BRANCH}]"
       else
               echo ""
       fi
}

show_virtual_env() {
  if [ -n "$VIRTUAL_ENV" ]; then
    local PROJECT="$(echo $VIRTUAL_ENV | rev | cut -d '/' -f 3 | rev)"
    echo "($PROJECT) "
  fi
}

PS1='$(show_virtual_env)'$PS1

#Git Completion For Mac
[ -f /usr/local/etc/bash_completion ] && . /usr/local/etc/bash_completion


alias tsl="~/otherRepos/sl_on_touchbar/sl"
