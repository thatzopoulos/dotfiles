alias pk="pkg list-all 2> /dev/null | grep -v -E '^Listing\.\.\.' | sed -r 's#^([^/]+)/.*\$#\\1#' | fzf --multi --preview 'pkg show {} 2> /dev/null | bat --color=always --pager=never --decorations=never --language=yaml' --preview-window='down:50%:wrap' | xargs -ro pkg install"


